package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("version")
public class VersionResource {

    final
    BuildProperties buildProperties;

    @Autowired
    public VersionResource(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @GetMapping
    public String getVersion()
    {
        return buildProperties.getVersion();
    }
}
